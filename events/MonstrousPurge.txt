namespace = monstrous_purge

#This is placeholder stuff and should be integrated with the greater racial minorities system

#Auto purge yo
country_event = {
	id = monstrous_purge.1
	title = monstrous_purge.1.t
	desc = monstrous_purge.1.d
	picture = BORDER_TENSION_eventPicture
	
	trigger = {
		#NOT = { has_country_modifier = monstrous_nation }	#will need to be changed for a better system eventually
		#Technically monsters should be able to purge each other?
		
		any_owned_province = {
			monstrous_culture = yes
			NOT = { culture_group = ROOT }
			is_in_capital_area = yes
				
			#NOT = { nationalism = 10 }
		}
		
		NOT = { is_year = 1650 }	#year when monstrous opinion is taken
		
		NOT = { has_idea_group = humanist_ideas }
	}
	
	immediate = {
		hidden_effect = {
			# random_owned_province = {
				# limit = { 
					# culture = ROOT
				# }
				# save_event_target_as = my_culture_province
			# }
			
			random_owned_province = {
				limit = {
					monstrous_culture = yes
					NOT = { culture_group = ROOT }
					is_in_capital_area = yes
					
					#NOT = { nationalism = 10 }
				}
				save_event_target_as = monstrous_culture_province
			}
		}
	}
	
	mean_time_to_happen = { 
		months = 200
		modifier = {
			factor = 0.75
			any_owned_province = {
				monstrous_culture = yes 
				OR = {
					region = dragon_coast_region
					superregion = escann_superregion
				}
			}
		}
		modifier = {
			factor = 0.75
			OR = {
				ruler_has_personality = conqueror_personality
				ruler_has_personality = expansionist_personality
				ruler_has_personality = malevolent_personality
			}
		}
		modifier = {
			factor = 2
			OR = {
				ruler_has_personality = benevolent_personality
				ruler_has_personality = tolerant_personality
				ruler_has_personality = just_personality
			}
		}
	}
	
	option = {
		name = monstrous_purge.1.a			
		ai_chance = {
			factor = 80
		}
		
		event_target:monstrous_culture_province = {
			change_culture = ROOT #event_target:my_culture_province
			change_religion = ROOT #event_target:my_culture_province
			
			add_devastation = 25
			
			add_nationalism = -10
		}
		
		add_dip_power = -10
		
	}
	
	option = {
		name = monstrous_purge.1.b
		ai_chance = {
			factor = 20
			modifier = {
				factor = 5
				OR = {
					ruler_has_personality = benevolent_personality
					ruler_has_personality = tolerant_personality
					ruler_has_personality = just_personality
				}
			}
		}
		add_dip_power = 10
		add_prestige = -1
	}
}