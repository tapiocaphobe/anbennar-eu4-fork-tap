government = monarchy
add_government_reform = feudalism_reform
government_rank = 1
primary_culture = castellyrian
religion = regent_court
technology_group = tech_cannorian
capital = 874
national_focus = DIP

1000.1.1 = { set_country_flag = mage_organization_decentralized_flag }

1400.1.2 = { set_country_flag = is_a_county }

1444.2.3 = {
	monarch = {
		name = "Carleon I"
		dynasty = "Blacktower"
		birth_date = 1391.4.7
		adm = 2
		dip = 4
		mil = 5
	}
	add_ruler_personality = strict_personality
}
