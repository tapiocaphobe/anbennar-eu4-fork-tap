government = theocracy
add_government_reform = monastic_order_reform
government_rank = 1
primary_culture = selphereg
religion = eordellon
technology_group = tech_eordand
capital = 2007

1431.7.5 = {
	monarch = {
		name = "Logar"
		birth_date = 1389.5.9
		adm = 2
		dip = 4
		mil = 4
	}
}
