#127 - Sassari | 

owner = A21
controller = A21
add_core = A21
culture = moon_elf
religion = regent_court

hre = no

base_tax = 6
base_production = 5
base_manpower = 2

trade_goods = glass
capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish

