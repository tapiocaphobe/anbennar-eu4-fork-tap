#234 - Hampshire | Damescrown

owner = A25
controller = A25
add_core = A25
culture = crownsman
religion = regent_court

hre = yes

base_tax = 12
base_production = 12
base_manpower = 6

trade_goods = paper
center_of_trade = 2

capital = ""

is_city = yes
fort_15th = yes 


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_orcish

add_permanent_province_modifier = {
	name = esmar_estuary_modifier
	duration = -1
}

add_permanent_province_modifier = {
	name = halfling_minority_oppressed_large
	duration = -1
}

add_permanent_province_modifier = {
	name = elven_minority_coexisting_large
	duration = -1
}