#359 - Faiyum

owner = A32
controller = A32
add_core = A32
culture = moon_elf
religion = elven_forebears

hre = no

base_tax = 7
base_production = 7
base_manpower = 2

trade_goods = livestock

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_orcish
