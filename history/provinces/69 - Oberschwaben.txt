# 69 - Oberschwaben | Rewanfork

owner = A01
controller = A01
add_core = A01
culture = high_lorentish
religion = regent_court
capital = ""
trade_goods = grain
base_tax = 5
base_production = 5
base_manpower = 2
is_city = yes
hre = no


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish

add_permanent_province_modifier = {
	name = dwarven_minority_coexisting_large
	duration = -1
}
