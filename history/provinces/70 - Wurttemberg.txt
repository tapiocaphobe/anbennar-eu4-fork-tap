owner = A01
controller = A01
add_core = A01

culture = high_lorentish
religion = regent_court

capital = "Ionnidar"

trade_goods = livestock

base_tax = 3

base_production = 3
base_manpower = 3

is_city = yes

hre = no

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish

