# No previous file for Tannu Uriankhai
owner = Z20
controller = Z20
add_core = Z20
culture = common_goblin
religion = goblinic_shamanism

hre = no

base_tax = 2
base_production = 2
base_manpower = 2

trade_goods = fur

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_gnomish

native_size = 50
native_ferocity = 5
native_hostileness = 8