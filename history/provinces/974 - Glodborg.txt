# No previous file for Glodborg
owner = Z09
controller = Z09
add_core = Z09
culture = olavish
religion = skaldhyrric_faith

hre = no

base_tax = 2
base_production = 2
base_manpower = 1

trade_goods = fish

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_gnomish
discovered_by = tech_giantkind