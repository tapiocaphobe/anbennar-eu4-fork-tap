# No previous file for Goa
owner = H06
controller = H06
add_core = H06
culture = caamas
religion = eordellon
capital = "Goa"

hre = no

base_tax = 2
base_production = 1
base_manpower = 2

trade_goods = unknown

native_size = 14
native_ferocity = 6
native_hostileness = 6