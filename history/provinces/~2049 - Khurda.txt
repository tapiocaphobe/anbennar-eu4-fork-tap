# No previous file for Khurda
owner = H08
controller = H08
add_core = H08
culture = selphereg
religion = eordellon
capital = "Khurda"

hre = no

base_tax = 2
base_production = 2
base_manpower = 2

trade_goods = unknown

native_size = 14
native_ferocity = 6
native_hostileness = 6